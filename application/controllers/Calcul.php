<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action extends CI_Controller {

	public function __construct() {

      parent::__construct();
      $this->load->helper('form');
      $this->load->library('session');
      $this->load->helper('url');
      $this->load->helper('html');
      $this->load->database();
      $this->load->library('form_validation');
      //load the model classes and helpers
      $this->load->model(['user_model', 'main_model', 'soldinitial_model', 'furnizori_model', 'date_model']);
      $this->load->helper('registru_helper'); 
          
    }

    public function index() {

    }

	public function suma_zi($table, $idzi) {
          return floatval($this->calcul_model->get_amount_by_day($table, $idzi));
     }

     public function cumul_curent($table, $idzi) {
          $firstday = $this->id_first_day_of_month($idzi);
          $lastday = $this->date_model->last_day_id();

          return floatval($this->calcul_model->cumul($table, $firstday, $lastday));          
     }
}