<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action extends CI_Controller {

	public function __construct() {

      parent::__construct();
      $this->load->helper('form');
      $this->load->library('session');
      $this->load->helper('url');
      $this->load->helper('html');
      $this->load->database();
      $this->load->library('form_validation');
      //load the model classes and helpers
      $this->load->model(['user_model', 'main_model', 'soldinitial_model', 'furnizori_model', 'date_model', 'calcul_model']);
      $this->load->helper('registru_helper'); 
          
    }

    public function index() {
      $data = ['IDZi' => 1, 'Suma' => 300, 'Factura' => 'FacturaNEW', 'IDFurnizor' => 1];
      /*$this->main_model->new_record('SumeCheltuieli', $data);*/
      var_dump($this->furnizori_model->get_furnizori(1) );
    }

    public function add_record($table) {
          $data = json_decode($this->input->post('record'));
          $this->main_model->new_record($table, $data);
     }

     public function edit_record($table, $id, $idzi) {
         if( !isset($_SESSION['userdata']) OR $idzi != $this->last_day_id() ) {
                  redirect("login");
            } else {
               $data = json_decode($this->input->post('record'));
               $this->main_model->edit_record($table, $data);
            }
     }

     public function delete_record($table, $id, $idzi) {
            if( !isset($_SESSION['userdata']) OR $idzi != $this->last_day_id() ) {
                  redirect("login");
            } else {
                  $this->main_model->delete_record($table, $id);
            }
     }

     public function edit_sold_initial($idzi, $sum) {
            //Daca nu e logat sau ultima zi din tabel nu coincide cu prima zi din luna
            if( !isset($_SESSION['userdata']) ) {
                  redirect("login");
            } else if( $this->date_model->id_first_day_by_id($idzi) != $idzi ){
                  echo 'Nu e prima zi din luna !';
            } else {
                  $this->soldinitial_model->edit_sold_initial($idzi, $sum);
            }
     }
}