<?php //Furnizor model class

Class Furnizori_model extends CI_Model {

	function __construct() {
		parent:: __construct();
		$this->load->database();
	}

	//Method to get providers by type(IDFurnizor(1, 2, 3))
    function get_furnizori($tip) {
    	$this->db->select('*');
    	$this->db->where('Tip', $tip);
    	$result = $this->db->get('Furnizori')->result_array()[0];

    	$final[] = ['ID' => intval($result['ID']), 'Tip' => intval($result['Tip']), 'Nume' => $result['Nume'] ];
    	return $final[0];
    }

	//Add furnizor record
	function new_furnizor($tip, $name) {
		$data = array(
			'Tip' => $tip,
			'Nume' => $name
			);

		$this->db->where('Tip', $tip);
        $this->db->where('Nume', $name);
        $this->db->get('Furnizori');

        $result = $this->db->insert('Furnizori', $data);

        return $result;    
	}

	function __destruct() {
        $this->db->close();
    }

}